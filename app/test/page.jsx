import { green } from "@mui/material/colors";
import { Button, Card, CardBody, CardFooter, CardHeader, Chip, Divider, Image, Link } from "@nextui-org/react";

export default function Page() {
  return (
    <div className="flex justify-center items-center h-screen">
      <Card className="w-[400px]">
        <CardHeader className="flex justify-between gap-3">
          <div className="flex flex-col">
            <p className="text-md font-bold">@vita_28</p>
            <p className="text-small text-default-500">805188190</p>
          </div>
          <div>
            <Chip
              className="capitalize cursor-pointer select-none"
              color={"success"}
              size="sm"
              variant="flat"
            >
              Approved
            </Chip>
          </div>
        </CardHeader>
        <Divider />
        <CardBody>
          <div className="flex justify-between">
            <div>First name</div>
            <div>Dara</div>
          </div>
        </CardBody>
        <Divider />
        <CardBody>
          <div className="flex justify-between">
            <div>Last name</div>
            <div>Sovita</div>
          </div>
        </CardBody>
        <Divider />
        <CardBody>
          <div className="flex justify-between">
            <div>Point</div>
            <div>02pt</div>
          </div>
        </CardBody>
        <Divider />
        <CardBody>
          <div className="flex justify-between">
            <div>Email</div>
            <div>sovita@gmail.com</div>
          </div>
        </CardBody>
        <Divider />
        <CardBody>
          <div className="flex justify-between items-center">
            <div>Review Payment</div>
            <div>
              <Chip
                className="capitalize cursor-pointer select-none"
                color={"primary"}
                size="sm"
                variant="flat"
              >
                View
              </Chip>
            </div>
          </div>
        </CardBody>
        <Divider />
        <CardBody>
          <div className="flex justify-between">
            <div>Registed</div>
            <div>6-January-2024</div>
          </div>
        </CardBody>
        <CardFooter className="px-6 pb-4">
          <Button className="m-auto w-full" color="primary" variant="bordered">
            Approve
          </Button>
        </CardFooter>
      </Card>
    </div>
  )
}