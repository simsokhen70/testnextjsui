"use client";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import { Field, Form, Formik } from "formik";
import Link from "next/link";
import { useState } from "react";
import * as Yup from "yup";
import { useRouter } from "next/navigation";
import LoadingComponent from "../component/loading/LoadingComponent";
import { Changepassword } from "../redux/services/user.service";
import CheckCircleOutlineIcon from "@mui/icons-material/CheckCircleOutline";
import ProtectedRoute from "../component/protected/ProtectedRoute";

const loginSchema = Yup.object().shape({
  currentPassword: Yup.string().required("Current password can't empty"),
  newPassword: Yup.string()
    .required("New password can't empty")
    .matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$/, {
      message:
        "Must contain at least one number and one uppercase and lowercase letter, and at least 6 or more characters",
    }),
  confirmPassword: Yup.string()
    .required("Confirm password can't empty")
    .oneOf([Yup.ref("newPassword"), null], "Passwords must match"),
});
export default function Page() {
  const [showPassword, setShowPassword] = useState(false);
  const [showModules, setShowModules] = useState(false);
  const [loading, setLoading] = useState(false);
  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };
  const [user, setUser] = useState({});
  const handleInput = (e) => {
    const { name, value } = e.target;
    setUser({ ...user, [name]: value });
  };
  const router = useRouter();
  const handleClose = () => {
    router.push("/dashboard");
    setShowModules(false);
  };
  return (
    <div>
      <ProtectedRoute>
        <Formik
          initialValues={{
            currentPassword: "",
            newPassword: "",
            confirmPassword: "",
          }}
          validationSchema={loginSchema}
          onSubmit={(values, { setFieldError }) => {
            console.log(values);
            setLoading(true);
            Changepassword(values).then((result) => {
              console.log(result);
              if (result.status === 200) {
                setLoading(false);
                values.currentPassword = "";
                values.newPassword = "";
                values.confirmPassword = "";
                setShowModules(true);
                // router.push("/dashboard");
              } else if (
                result.response.data.error ===
                "Current Password isn't correct. Please Try Again."
              ) {
                setLoading(false);
                setFieldError("currentPassword", "Invalid current password");
              } else if (
                result.response.data.error ===
                "Your new password is still the same as your old password"
              ) {
                setLoading(false);
                setFieldError(
                  "newPassword",
                  "Your new password is still the same as your old password"
                );
              } else if (
                result.response.data.error ===
                "Your confirm password does not match with your new password"
              ) {
                setLoading(false);
                setFieldError(
                  "confirmPassword",
                  "Your confirm password does not match with your new password"
                );
              }
            });
          }}
        >
          {({ errors, touched }) => (
            <div className="relative flex flex-col items-center justify-center min-h-screen overflow-hidden">
              <div className="w-full p-6 bg-white rounded-md shadow-md lg:max-w-xl">
                <h1 className="text-3xl font-bold text-center text-gray-700">
                  Change Password
                </h1>
                <Form className="mt-6" onChange={handleInput}>
                  <div className="mb-4">
                    <label
                      htmlFor="text"
                      className="block text-sm font-semibold text-gray-800"
                    >
                      Current Password
                    </label>
                    <Field
                      type="password"
                      placeholder="*********"
                      name="currentPassword"
                      className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border rounded-md focus:border-gray-400 focus:ring-gray-300 focus:outline-none focus:ring focus:ring-opacity-40"
                    />
                    {errors.currentPassword && touched.currentPassword ? (
                      <div className="text-red-500 text-[14.4px] mt-2">
                        {errors.currentPassword}
                      </div>
                    ) : null}
                  </div>
                  <div className="mb-4">
                    <label
                      htmlFor="text"
                      className="block text-sm font-semibold text-gray-800"
                    >
                      New Password
                    </label>
                    <Field
                      type="password"
                      placeholder="*********"
                      name="newPassword"
                      className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border rounded-md focus:border-gray-400 focus:ring-gray-300 focus:outline-none focus:ring focus:ring-opacity-40"
                    />
                    {errors.newPassword && touched.newPassword ? (
                      <div className="text-red-500 text-[14.4px] mt-2">
                        {errors.newPassword}
                      </div>
                    ) : null}
                  </div>
                  <div className="mb-4">
                    <label
                      htmlFor="text"
                      className="block text-sm font-semibold text-gray-800"
                    >
                      Confirm Password
                    </label>
                    <Field
                      type="password"
                      placeholder="*********"
                      name="confirmPassword"
                      className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border rounded-md focus:border-gray-400 focus:ring-gray-300 focus:outline-none focus:ring focus:ring-opacity-40"
                    />
                    {errors.confirmPassword && touched.confirmPassword ? (
                      <div className="text-red-500 text-[14.4px] mt-2">
                        {errors.confirmPassword}
                      </div>
                    ) : null}
                  </div>
                  {/* <div className="mb-2 relative">
                <label
                  htmlFor="password"
                  className="block text-sm font-semibold text-gray-800"
                >
                  Password
                </label>
                <Field
                  type={showPassword ? "text" : "password"}
                  placeholder="Password"
                  name="password"
                  // type="password"
                  className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border rounded-md focus:border-gray-400 focus:ring-gray-300 focus:outline-none focus:ring focus:ring-opacity-40"
                />
                {user.password && (
                  <FontAwesomeIcon
                    icon={showPassword ? faEye : faEyeSlash}
                    onClick={togglePasswordVisibility}
                    className="absolute inset-y-0 top-10 right-0 flex items-center pr-3 text-iconcolor cursor-pointer"
                  />
                )}
                {errors.password && touched.password ? (
                  <div className="text-red-500 text-[14.4px]">
                    {errors.password}
                  </div>
                ) : null}
              </div> */}
                  {/* <Link
                href="/forget"
                className="text-xs text-blue-600 hover:underline"
              >
                Forget Password?
              </Link> */}
                  <div className="mt-2">
                    <button
                      type="submit"
                      className="w-full px-4 py-2 tracking-wide text-white transition-colors duration-200 transform bg-gray-700 rounded-md hover:bg-gray-600 focus:outline-none focus:bg-gray-600"
                    >
                      {loading ? <LoadingComponent /> : "Confirm"}
                    </button>
                  </div>
                  <div className="mt-4">
                    <Link
                      href="/dashboard"
                      type="button"
                      className="w-full px-4 py-1 tracking-wide outline-2 outline outline-offset-1 outline-red-500 transition-colors duration-200 transform bg-white text-gray-700 rounded-md hover:bg-red-600 hover:text-white focus:outline-none focus:bg-gray-600"
                    >
                      <p className="w-full text-center">Cancel</p>
                    </Link>
                  </div>
                </Form>
              </div>
            </div>
          )}
        </Formik>
        {showModules && (
          <div className="flex absolute justify-center items-center z-100 overflow-x-hidden overflow-y-hidden w-full md:inset-0 h-[calc(100%-1rem)] md:h-full backdrop-blur-[2px] bg-black/10">
            <div className="bg-white border w-80 h-auto rounded-[10px] shadow-myshadow">
              <div className="bg-green-500 text-white inline-flex w-full justify-end py-[19px] px-4 rounded-t-[10px]"></div>
              <div className="flex flex-col justify-center items-center mt-[46px] ">
                <div className="text-[20px] leading-normal text-center text-gray-600 ">
                  Password changed successfully
                </div>
                <div className="w-full h-[1px] bg-[#DCDCE4] my-[30px]"></div>
                <div className="flex justify-center items-center gap-5 mb-[30px] self-stretch ">
                  <button
                    onClick={() => handleClose()}
                    className=" rounded-[6px] w-[8rem] text-white bg-gray-600 hover:bg-white hover:text-secondary outline outline-2 flex justify-center items-center gap-[10px] px-[16px] py-[8px] hover:outline-secondary "
                  >
                    Done
                  </button>
                </div>
              </div>
            </div>
          </div>
        )}
      </ProtectedRoute>
    </div>
  );
}
