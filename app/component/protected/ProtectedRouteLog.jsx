"use client";
import { useRouter } from "next/navigation";
import { useEffect } from "react";

const ProtectedRouteLog = ({ children }) => {
  const router = useRouter();
  useEffect(() => {
    // Check if we are on the client-side before accessing localStorage
    if (typeof window !== 'undefined') {
      const token = localStorage.getItem("Token");
      if (token) {
        router.push("/dashboard")
      }
    }
  }, [router]);
  return <>{children}</>;

};

export default ProtectedRouteLog;
