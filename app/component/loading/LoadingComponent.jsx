"use client";
import React, { useState } from "react";
import { ClipLoader } from "react-spinners";

export default function LoadingComponent() {
  let [color, setColor] = useState("#ffffff");
  return (
    <div>
    <ClipLoader
        color={color}
        size={22}
        aria-label="Loading Spinner"
        data-testid="loader"
      />
    </div>
  );
}
