"use client";
import React, { useState } from "react";
import { ClipLoader } from "react-spinners";

export default function LoadingComponentGray() {
  let [color, setColor] = useState("#ffffff");
  return (
    <div>
    <ClipLoader
        color={color}
        size={96}
        width={100}
        aria-label="Loading Spinner"
        data-testid="loader"
      />
    </div>
  );
}