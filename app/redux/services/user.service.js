import { API } from "../../utils/util";

export const GetAllUser = async () => {
  try {
    const response = await API.get(`/admin/getAllUser?page=0&size=10000`);
    return response;
  } catch (error) {
    return error;
  }
};

export const GetApproveUsers = async () => {
  try {
    const response = await API.get(`/admin/getAllUserApprove?page=0&size=100000`);
    return response;
  } catch (error) {
    return error;
  }
};

export const GetUnApproveUsers = async () => {
  try {
    const response = await API.get(`/admin/getAllUserUnApprove?page=0&size=100000`);
    return response;
  } catch (error) {
    return error;
  }
};

export const GetUserById = async (id) => {
  try {
    const response = await API.get(`/admin/getUserById/${id}`);
    return response;
  } catch (error) {
    return error;
  }
};

export const ApproveUser = async (id) => {
  try {
    const response = await API.put(`/admin/Approve/${id}`);
    return response;
  } catch (error) {
    return error;
  }
};

export const Changepassword = async (pass) => {
  try {
    const response = await API.put(`/auth/change-password`, pass);
    return response;
  } catch (error) {
    return error;
  }
};
export const DeleteReqUser = async (id) => {
  try {
    const response = await API.delete(`/admin/Delete/${id}`);
    return response;
  } catch (error) {
    return error;
  }
};
